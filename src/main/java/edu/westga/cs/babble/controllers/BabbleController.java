package edu.westga.cs.babble.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import edu.westga.cs.babble.model.EmptyTileBagException;
import edu.westga.cs.babble.model.PlayedWord;
import edu.westga.cs.babble.model.Tile;
import edu.westga.cs.babble.model.TileBag;
import edu.westga.cs.babble.model.TileNotInGroupException;
import edu.westga.cs.babble.model.TileRack;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;


public class BabbleController implements Initializable {

	private IntegerProperty score = new SimpleIntegerProperty(0);
	private TileBag tileBag = new TileBag();
	private TileRack tileRack = new TileRack();
	private PlayedWord yourWord = new PlayedWord();
	
    @FXML
    private TextField scoreBar;

    @FXML
    private Button playWordButton;

    @FXML
    private Button resetButton;

    @FXML
    private ListView<Tile> yourWordView;

    @FXML
    private ListView<Tile> tilesView;

	
	@Override
	public void initialize(URL url, ResourceBundle rb)	{
		this.populateTileRack();
		this.displayTileRack();
		this.displayWordToPlay();
		this.setScore();
		this.handleResetButton();
		this.handlePlayWordButton();
		
	}
	
	/*
	 * Sets mouse click event listeners for the play word button
	 */
	private void handlePlayWordButton()	{
		WordDictionary dictionary = new WordDictionary();
		playWordButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent me)	{
				if (dictionary.isValidWord(yourWord.getHand()) && yourWord.matches(yourWord.getHand())) {
					score.set(score.getValue() + yourWord.getScore());
					yourWord.tiles().removeAll(yourWord.tiles());
					populateTileRack();
				} else {
					Alert invalidWordAlert = new Alert(AlertType.INFORMATION);
					invalidWordAlert.setTitle("Invalid Word Alert");
					invalidWordAlert.setHeaderText("Invalid Word Alert");
					invalidWordAlert.setContentText("That is an invalid word.");
					
					invalidWordAlert.showAndWait();
				}
			}
		});
	}
	
	/*
	 * Sets mouse click event listeners for the reset button
	 */
	private void handleResetButton()	{
		resetButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent me)	{
				tileRack.tiles().addAll(yourWord.tiles());
				yourWord.clear();
			}
		});
	}
	
	/*
	 * Populates the tile rack with new tiles
	 */
	private void populateTileRack() {
		try {
			int tilesToGet = tileRack.getNumberOfTilesNeeded();
			for(int i = 0; i < tilesToGet; i++) {
				this.tileRack.append(this.tileBag.drawTile());
			}
		} catch (EmptyTileBagException etbe)	{
			System.out.println("");
		}
	}
	
	/*
	 * Sets the cell factory
	 * Sets mouse click event handler to remove tile and append it to the word view
	 */
	private void displayTileRack()	{
		tilesView.setItems(tileRack.tiles());
		tilesView.setCellFactory(new Callback<ListView<Tile> , ListCell<Tile>>(){
			@Override
			public ListCell<Tile> call(ListView<Tile> list)	{
				return new TileCell();
			}
		});
		
		tilesView.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent me)	{
				try {
					Tile currentTile = tilesView.getSelectionModel().getSelectedItem(); 
					
					yourWord.append(currentTile);
					
					tileRack.remove(currentTile);
				} catch (TileNotInGroupException tnige)	{
					System.out.println("Tile Not in Group Exception");
				}
			}
		});
	}
	
	/*
	 * Sets the cell factory
	 * Sets mouse click event handler to remove the tile and append it to the tile rack
	 */
	private void displayWordToPlay()	{
		
		yourWordView.setItems(yourWord.tiles());
		yourWordView.setCellFactory(new Callback<ListView<Tile> , ListCell<Tile>>(){
			@Override
			public ListCell<Tile> call(ListView<Tile> list)	{
				return new TileCell();
			}
		});
		
		yourWordView.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent me)	{
				try {
					Tile currentTile = yourWordView.getSelectionModel().getSelectedItem(); 
					
					tileRack.append(currentTile);
					
					yourWord.remove(currentTile);
				} catch (TileNotInGroupException tnige)	{
					System.out.println("Tile Not in Group Exception");
				}
			}
		});
	}
	
	/*
	 * Binds the score  in the score bar to a simple integer property
	 */
	private void setScore() {
		scoreBar.textProperty().bind(score.asString());
	}
	
	private class TileCell extends ListCell<Tile> {
		@Override
		public void updateItem(Tile item, boolean empty)	{
			super.updateItem(item, empty);
			if (item!= null)	{
				setText(Character.toString(item.getLetter()));
				setAccessibleText(Character.toString(item.getLetter()));
			} else {
				setText(null);
			}
		}
	}
	
}
