package edu.westga.cs.babble.controllers;

import static org.junit.Assert.*;
//import junit.framework.Assert;
//import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.westga.cs.babble.controllers.WordDictionary;

public class TestWordDictionary {

	WordDictionary dictionary;
	
	@Before
	public void setUp() throws Exception {
		this.dictionary = new WordDictionary();
	}

	
	@Test
	public void stringExpandShouldBeValid() {
		assertTrue(this.dictionary.isValidWord("Expand"));
	}
	
	@Test
	public void stringBugblatShouldNotBeValid() {
		assertFalse(this.dictionary.isValidWord("Bugblat"));
	}
	
	@Test
	public void emptyStringShouldNotBeValid() {
		assertFalse(this.dictionary.isValidWord(""));
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldNotAcceptNull() {
		this.dictionary.isValidWord(null);
	}
}
