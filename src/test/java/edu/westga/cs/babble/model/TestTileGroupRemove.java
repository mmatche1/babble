/**
 * 
 */
package edu.westga.cs.babble.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/** Tests TileGroup#remove()
* 
* @author Mandi Matchett
* @version 09/02/2017
*
*/
public class TestTileGroupRemove {
	
	TileBag tileBag;
	TileGroup dtg;
	Tile aTile;
	Tile bTile;
	Tile cTile;

	@Before
	public void setUp() throws Exception {
		this.dtg = new DummyTileGroup();
		this.tileBag = new TileBag();
		this.aTile = new Tile('A');
		this.bTile = new Tile('B');
		this.cTile = new Tile('C');
	}

	@Test(expected = TileNotInGroupException.class)
	public void canNotRemoveFromEmptyTileGroup() throws TileNotInGroupException{
		this.dtg.remove(this.aTile);
	}
	
	@Test(expected = TileNotInGroupException.class)
	public void canNotRemoveTileNotInTileGroup() throws TileNotInGroupException{
		this.dtg.append(this.aTile);
		this.dtg.remove(this.bTile);
	}
	
	@Test
	public void canRemoveOnlyTileInTileGroup() throws TileNotInGroupException{
		this.dtg.append(this.aTile);
		this.dtg.remove(this.aTile);
		assertTrue(this.dtg.getHand().length() == 0);
		assertTrue(this.dtg.getHand().equals(""));
	}
	
	@Test
	public void canRemoveFirstOfManyTilesInTileGroup() throws TileNotInGroupException{
		this.dtg.append(this.aTile);
		this.dtg.append(this.bTile);
		this.dtg.append(this.cTile);
		this.dtg.remove(this.aTile);
		assertTrue(this.dtg.getHand().length() == 2);
		assertTrue(this.dtg.getHand().equals("BC"));
	}
	
	@Test
	public void canRemoveLastOfManyTilesInTileGroup() throws TileNotInGroupException{
		this.dtg.append(this.aTile);
		this.dtg.append(this.bTile);
		this.dtg.append(this.cTile);
		this.dtg.remove(this.cTile);
		assertTrue(this.dtg.getHand().length() == 2);
		assertTrue(this.dtg.getHand().equals("AB"));
	}
	
	@Test
	public void canRemoveMiddleOfManyTilesInTileGroup() throws TileNotInGroupException{
		this.dtg.append(this.aTile);
		this.dtg.append(this.bTile);
		this.dtg.append(this.cTile);
		this.dtg.remove(this.bTile);
		assertTrue(this.dtg.getHand().length() == 2);
		assertTrue(this.dtg.getHand().equals("AC"));
	}
	
	@Test
	public void canRemoveMultipleTilesFromTileGroup() throws TileNotInGroupException{
		this.dtg.append(this.aTile);
		this.dtg.append(this.bTile);
		this.dtg.append(this.cTile);
		this.dtg.remove(this.aTile);
		this.dtg.remove(this.cTile);
		assertTrue(this.dtg.getHand().length() == 1);
		assertTrue(this.dtg.getHand().equals("B"));
	}
	
	@Test
	public void doesNotRemoveDuplicateTileFromTileGroup() throws TileNotInGroupException{
		Tile a2Tile = new Tile('A');
		this.dtg.append(this.aTile);
		this.dtg.append(this.bTile);
		this.dtg.append(this.cTile);
		this.dtg.append(a2Tile);
		this.dtg.remove(this.aTile);
		assertTrue(this.dtg.getHand().length() == 3);
		assertTrue(this.dtg.getHand().equals("BCA"));
	}
	
	private class DummyTileGroup extends TileGroup {
		/**
		 * Dummy class created for testing TileGroup abstract methods
		 */
	}

}
