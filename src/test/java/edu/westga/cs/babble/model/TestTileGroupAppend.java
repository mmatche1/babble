/**
 * 
 */
package edu.westga.cs.babble.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/** Tests TileGroup#append()
* 
* @author Mandi Matchett
* @version 09/02/2017
*
*/
public class TestTileGroupAppend {

	TileBag tileBag;
	TileGroup dtg;
	Tile aTile;
	Tile bTile;
	Tile cTile;

	@Before
	public void setUp() throws Exception {
		this.dtg = new DummyTileGroup();
		this.tileBag = new TileBag();
		this.aTile = new Tile('A');
		this.bTile = new Tile('B');
		this.cTile = new Tile('C');
	}
	

	@Test(expected = IllegalArgumentException.class)
	public void shouldNotAllowNull() {
		this.dtg.append(null);
	}
	
	@Test
	public void emptyGroupShouldBeEmpty() {
		assertTrue(this.dtg.getHand() == "");
	}
	
	@Test
	public void shouldHaveOneTileInTileGroup() throws EmptyTileBagException {
		this.dtg.append(this.aTile);
		assertTrue(dtg.getHand().length() == 1);
		assertTrue(dtg.getHand().equals("A"));
	}
	
	@Test
	public void shouldHaveManyTilesInTileGroup() throws EmptyTileBagException {
		this.dtg.append(this.aTile);
		this.dtg.append(this.bTile);
		this.dtg.append(this.cTile);
		//String tiles = Character.toString(aTile.getLetter()) + Character.toString(bTile.getLetter()) + Character.toString(cTile.getLetter());
		assertTrue(this.dtg.getHand().length() == 3);
		assertTrue(this.dtg.getHand().equals("ABC"));
	}
	
	@Test
	public void shouldHaveManyTilesIncludingDuplicatesInTileGroup() {
		Tile a2Tile = new Tile('A');
		Tile b2Tile = new Tile('B');
		this.dtg.append(this.aTile);
		this.dtg.append(this.bTile);
		this.dtg.append(this.cTile);
		this.dtg.append(a2Tile);
		this.dtg.append(b2Tile);
		assertTrue(this.dtg.getHand().length() == 5);
		assertTrue(this.dtg.getHand().equals("ABCAB"));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void canNotAddSameTileTwice() throws EmptyTileBagException {
		this.dtg.append(this.aTile);
		this.dtg.append(this.bTile);
		this.dtg.append(this.cTile);
		this.dtg.append(this.aTile);
	}

	private class DummyTileGroup extends TileGroup {
		/**
		 * Dummy class created for testing TileGroup abstract methods
		 */
	}
}
