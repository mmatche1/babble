/**
 * 
 */
package edu.westga.cs.babble.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/** Tests PlayedWord#getScore()()
* 
* @author Mandi Matchett
* @version 09/02/2017
*
*/
public class TestPlayedWordGetScore {

	PlayedWord word;
	Tile aTile;
	Tile bTile;
	Tile sTile;
	
	@Before
	public void setUp() throws Exception {
		this.word = new PlayedWord();
		this.aTile = new Tile('A');
		this.bTile = new Tile('B');
		this.sTile = new Tile('S');
	}
	
	@Test
	public void emptyWordShouldHaveScoreOfZero() {
		assertTrue(word.getScore() == 0);
	}
	
	@Test
	public void scoreAOneTileWord() {
		word.append(this.aTile);
		assertTrue(word.getScore() == 1);
	}
	
	@Test
	public void scoreAWordWithMultipleDifferentTiles() {
		this.word.append(aTile);
		this.word.append(bTile);
		this.word.append(sTile);
		assertTrue(word.getScore() == 5);
	}
	
	@Test
	public void scoreAWordcontainingDuplicateTiles() {
		Tile a2Tile = new Tile('A');
		this.word.append(aTile);
		this.word.append(bTile);
		this.word.append(a2Tile);
		assertTrue(word.getScore() == 5);
	}

}
