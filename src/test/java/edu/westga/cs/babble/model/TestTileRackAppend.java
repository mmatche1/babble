/**
 * 
 */
package edu.westga.cs.babble.model;

//import static org.junit.Assert.*;
import org.junit.Test;

/** Tests TileRack#append()
* 
* @author Mandi Matchett
* @version 09/02/2017
*
*/
public class TestTileRackAppend {

	@Test(expected = TileRackFullException.class)
	public void shouldNotAppendToFullRack() throws EmptyTileBagException {
		TileBag tileBag = new TileBag();
		TileRack tileRack = new TileRack();
		for(int i = 0; i < 8; i++)	{
			Tile tile = tileBag.drawTile();
			tileRack.append(tile);
		}
	}

}
