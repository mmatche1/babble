/**
 * 
 */
package edu.westga.cs.babble.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/** Tests PlayedWord#matches()
* 
* @author Mandi Matchett
* @version 09/02/2017
*
*/
public class TestPlayedWordMatches {

	PlayedWord word;
	Tile aTile = new Tile('A');
	Tile cTile = new Tile('C');
	Tile tTile = new Tile('T');
	
	@Before
	public void setUp() throws Exception {
		this.word = new PlayedWord();
	}
	
	@Test
	public void hasTilesForAMultipleLetterWord() {
		this.word.append(this.cTile);
		this.word.append(this.aTile);
		this.word.append(this.tTile);
		assertTrue(this.word.matches("CAT"));
	}
	
	@Test
	public void hasTilesForASingleLetterWord() {
		this.word.append(this.aTile);
		assertTrue(this.word.matches("A"));
	}
	
	@Test
	public void cannotMatchWordWhenTilesAreScrambled() {
		this.word.append(this.cTile);
		this.word.append(this.tTile);
		this.word.append(this.aTile);
		assertFalse(this.word.matches("CAT"));
	}
	
	@Test
	public void cannotMatchWordIfInsufficientTiles() {
		this.word.append(this.cTile);
		this.word.append(this.aTile);
		assertFalse(this.word.matches("CAT"));
	}
	
	@Test
	public void cannotMatchWordWithDuplicateLetters() {
		Tile a2Tile = new Tile('A');
		this.word.append(this.cTile);
		this.word.append(this.aTile);
		this.word.append(a2Tile);
		this.word.append(this.tTile);
		assertFalse(this.word.matches("CAT"));
	}
	
	@Test
	public void nonEmptyWordShouldNotMatchEmptyText() {
		this.word.append(this.cTile);
		this.word.append(this.aTile);
		this.word.append(this.tTile);
		assertFalse(this.word.matches(""));
	}

}
