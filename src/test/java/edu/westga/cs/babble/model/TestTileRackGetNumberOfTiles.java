/**
 * 
 */
package edu.westga.cs.babble.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/** Tests TileRack#getNumberOfTilesNeeded()
* 
* @author Mandi Matchett
* @version 09/02/2017
*
*/
public class TestTileRackGetNumberOfTiles {

	TileRack tileRack;
	TileBag tileBag;
	Tile aTile;
	Tile bTile;
	Tile cTile;
	
	@Before
	public void setUp() throws Exception {
		this.tileRack = new TileRack();
		this.tileBag = new TileBag();
		this.aTile = new Tile('A');
		this.bTile = new Tile('B');
		this.cTile = new Tile('C');
	}
	
	@Test
	public void emptyTileRackShouldNeedMaxSizeNumberOfTiles() {
		assertTrue(this.tileRack.getNumberOfTilesNeeded() == TileRack.MAX_SIZE);
	}
	
	@Test
	public void tileRackWithOneTileShouldNeedMaxSizeMinusOneTiles() {
		this.tileRack.append(this.aTile);
		assertTrue(this.tileRack.getNumberOfTilesNeeded() == TileRack.MAX_SIZE - 1);
	}
	
	@Test
	public void tileRackWithSeveralTilesShouldNeedSomeTiles() {
		this.tileRack.append(this.aTile);
		this.tileRack.append(this.bTile);
		this.tileRack.append(this.cTile);
		assertTrue(this.tileRack.getNumberOfTilesNeeded() == TileRack.MAX_SIZE - 3);
	}
	
	@Test
	public void fullRackNeedsZeroTiles() throws EmptyTileBagException {
		for (int i = 0; i < TileRack.MAX_SIZE; i++)	{
			Tile newTile = this.tileBag.drawTile();
			this.tileRack.append(newTile);
		}
		assertTrue(this.tileRack.getNumberOfTilesNeeded() == 0);
	}

}
