/**
 * 
 */
package edu.westga.cs.babble.model;

import static org.junit.Assert.*;

import org.junit.Test;

/** Tests TileBag constructor
* 
* @author Mandi Matchett
* @version 09/02/2017
*
*/
public class TestTileConstructor {
	
	Tile test;

	@Test(expected = IllegalArgumentException.class)
	public void shouldNotAllowNonLetters() {
		this.test = new Tile('9');
		
	}
	
	@Test
	public void shouldCreateOnePointTiles() {
		String onePointLetters = "EAIONRTLSU";
		for (int i = 0; i < onePointLetters.length(); i++)	{
			Tile lowerTile = new Tile(Character.toLowerCase(onePointLetters.charAt(i)));
			Tile upperTile = new Tile(onePointLetters.charAt(i));
			assertTrue(lowerTile.getPointValue() == 1);
			assertTrue(upperTile.getPointValue() == 1);
		}
	}
	
	@Test
	public void shouldCreateTwoPointTiles() {
		String twoPointLetters = "DG";
		for (int i = 0; i < twoPointLetters.length(); i++)	{
			Tile lowerTile = new Tile(Character.toLowerCase(twoPointLetters.charAt(i)));
			Tile upperTile = new Tile(twoPointLetters.charAt(i));
			assertTrue(lowerTile.getPointValue() == 2);
			assertTrue(upperTile.getPointValue() == 2);
		}
	}
	
	@Test
	public void shouldCreateThreePointTiles() {
		String threePointLetters = "BCMP";
		for (int i = 0; i < threePointLetters.length(); i++)	{
			Tile lowerTile = new Tile(Character.toLowerCase(threePointLetters.charAt(i)));
			Tile upperTile = new Tile(threePointLetters.charAt(i));
			assertTrue(lowerTile.getPointValue() == 3);
			assertTrue(upperTile.getPointValue() == 3);
		}
	}
	
	@Test
	public void shouldCreateFourPointTiles() {
		String fourPointLetters = "FHVWY";
		for (int i = 0; i < fourPointLetters.length(); i++)	{
			Tile lowerTile = new Tile (Character.toLowerCase(fourPointLetters.charAt(i)));
			Tile upperTile = new Tile(fourPointLetters.charAt(i));
			assertTrue(lowerTile.getPointValue() == 4);
			assertTrue(upperTile.getPointValue() == 4);
		}
	}
	
	@Test
	public void shouldCreateFivePointTiles() {
		Tile lowerTile = new Tile('k');
		Tile upperTile = new Tile('K');
		assertTrue(lowerTile.getPointValue() == 5);
		assertTrue(upperTile.getPointValue() == 5);
	}
	
	@Test
	public void shouldCreateEightPointTiles() {
		String eightPointLetters = "JX";
		for (int i = 0; i < eightPointLetters.length(); i++)	{
			Tile lowerTile = new Tile(Character.toLowerCase(eightPointLetters.charAt(i)));
			Tile upperTile = new Tile(eightPointLetters.charAt(i));
			assertTrue(lowerTile.getPointValue() == 8);
			assertTrue(upperTile.getPointValue() == 8);
		}
	}
	
	@Test
	public void shouldCreateTenPointTiles() {
		String tenPointLetters = "QZ";
		for (int i = 0; i < tenPointLetters.length(); i++)	{
			Tile lowerTile = new Tile(Character.toLowerCase(tenPointLetters.charAt(i)));
			Tile upperTile = new Tile(tenPointLetters.charAt(i));
			assertTrue(lowerTile.getPointValue() == 10);
			assertTrue(upperTile.getPointValue() == 10);
		}
	}

}
