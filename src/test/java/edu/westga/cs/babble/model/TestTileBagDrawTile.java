/**
 * 
 */
package edu.westga.cs.babble.model;

import static org.junit.Assert.*;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;

/** Tests TileBag#drawTile()
 * 
 * @author Mandi Matchett
 * @version 09/02/2017
 *
 */
public class TestTileBagDrawTile {

	TileBag tileBag;
	
	@Before
	public void setUp() throws Exception {
		this.tileBag = new TileBag();
	}
	
	@Test
	public void canDrawAllTiles() throws EmptyTileBagException {
		for (int i = 0; i < 98; i++) {
			this.tileBag.drawTile();
		}
		assertTrue(this.tileBag.isEmpty());
	}
	
	@Test(expected = EmptyTileBagException.class)
	public void canNotDrawTooManyTiles() throws EmptyTileBagException {
		for (int i = 0; i < 99; i++) {
			this.tileBag.drawTile();
		}
	}
	
	@Test
	public void hasProperTileDistribution() throws EmptyTileBagException {
		int[] count = new int[26];
		int[] correct = {9, 2, 2, 4, 12, 2, 3, 2, 9, 1, 1, 4, 2, 6, 8, 2, 1, 6, 4, 6, 4, 2, 2, 1, 2, 1};
		for (int i = 0; i < 98; i++) {
			Tile tile = this.tileBag.drawTile();
			if (tile.getLetter() == 'A') { count[0]++; }
			else if (tile.getLetter() == 'B') { count[1]++; }
			else if (tile.getLetter() == 'C') { count[2]++; }
			else if (tile.getLetter() == 'D') { count[3]++; }
			else if (tile.getLetter() == 'E') { count[4]++; }
			else if (tile.getLetter() == 'F') { count[5]++; }
			else if (tile.getLetter() == 'G') { count[6]++; }
			else if (tile.getLetter() == 'H') { count[7]++; }
			else if (tile.getLetter() == 'I') { count[8]++; }
			else if (tile.getLetter() == 'J') { count[9]++; }
			else if (tile.getLetter() == 'K') { count[10]++; }
			else if (tile.getLetter() == 'L') { count[11]++; }
			else if (tile.getLetter() == 'M') { count[12]++; }
			else if (tile.getLetter() == 'N') { count[13]++; }
			else if (tile.getLetter() == 'O') { count[14]++; }
			else if (tile.getLetter() == 'P') { count[15]++; }
			else if (tile.getLetter() == 'Q') { count[16]++; }
			else if (tile.getLetter() == 'R') { count[17]++; }
			else if (tile.getLetter() == 'S') { count[18]++; }
			else if (tile.getLetter() == 'T') { count[19]++; }
			else if (tile.getLetter() == 'U') { count[20]++; }
			else if (tile.getLetter() == 'V') { count[21]++; }
			else if (tile.getLetter() == 'W') { count[22]++; }
			else if (tile.getLetter() == 'X') { count[23]++; }
			else if (tile.getLetter() == 'Y') { count[24]++; }
			else if (tile.getLetter() == 'Z') { count[25]++; }
		}
		boolean equal = Arrays.equals(count, correct);
		assertTrue(equal);
	}

}
